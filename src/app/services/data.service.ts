import { Injectable } from '@angular/core';
import { Chart } from "chart.js";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  crearTabla = () => {
    fetch('../../assets/data/data_test.json')
    .then(function(data){
        
        
        return data.json();
    })
    .then(function(datos){
        let productos = datos.products;
        console.log(productos);
        
        document.getElementById("countUsers").textContent = datos.products.length;
        let productsTable = `<table id="productTable" class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">ID</th>
                                        <th scope="col">MUEBLE</th>
                                        <th scope="col">CATEGORIA</th>
                                        <th scope="col">PRECIO</th>
                                        <th scope="col">DISPONIBILIDAD</th>
                                    </tr>
                                </thead>
                                <tbody>`
        for(let p of productos){
            let id = p.id;
            let mueble = p.title;
            let categoria = datos.categories[p.category]
            let precio = p.price;
            let disponible = datos.available[p.available];

            let codigoHtml = `<tr>
                                <th scope="row">${id}</th>
                                <td>${mueble}</td>
                                <td>${categoria}</td>
                                <td>${precio}</td>
                                <td>${disponible}</td>
                                <td><a href="#"><i class="mr-4 fas fa-trash-alt"></i></a>
                                <a href="#"> <i class="fas fa-pen"></i></a>
                                </td>
                              </tr>`
            productsTable += codigoHtml;
        }
        productsTable += `</tbody>
                          </table>`

        document.querySelector(".tabla .row #table .card-body").innerHTML += productsTable;

        

    }).catch((error)=>{
        console.log("Ha ocurrido un error en: " + error.message);
    });
        
    

  }

  getDataEstadisticas(){
    
      fetch('../assets/data/data_test.json')
      .then(function(data){
          return data.json();
      })
      .then(function(datos){
          
          /*Conte de productos por Categoria*/
          let productos = datos.products;
          let contSalas = 0;
          let conComedores = 0;
          for(let p of productos){
              
              if(p.category === 1){
                  contSalas++;
              }else{
                  conComedores++;
              }
          }
  
          let canvas:any = document.getElementById('categoryChart');
  
          let chart = new Chart(canvas,{
              type: 'bar',
              data:{
                  labels: ["Sala", "Comedor"],
                  datasets: [{
                      label: "",
                      data: [contSalas,conComedores],
                      backgroundColor: [
                          'rgba(255, 99, 132, 0.2)',
                          'rgba(54, 162, 235, 0.2)'
                      ],
                      borderColor:[
                          'rgba(255, 99, 132, 1)',
                          'rgba(54, 162, 235, 1)'
                      ],
                      borderWidth: 1
                  }]
              },
              options: {
                  scales: {
                      yAxes: [{
                          ticks: {
                              beginAtZero: true
                          }
                      }]
                  },
                  legend: {
                      display: true
                  },
                  title:{
                      display: true,
                      text: 'Cantidad de Muebles por Categoria'
                  }
              }
          })
      })
      .catch(function(e){
          console.log("Ha habido un error con el fetch: "+e.message)
      })
  
  }


}
